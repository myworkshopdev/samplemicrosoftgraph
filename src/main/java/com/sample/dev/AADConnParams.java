package com.sample.dev;

 /**
  * @author mgmartinez
  * @version 1.0
  */
public class AADConnParams {
    private String aadClientId;                 // AppKey
    private String aadClientSecret;             // Client Secret
    private String aadEndpointInst;             // Access Token Request URL
    private String aadTenant;                   // Tenant
    private String aadGraphApi;                 // Scope

    public AADConnParams(String aadClientId, String aadClientSecret, String aadEndpointInst, String aadTenant,
                         String aadGraphApi) {
        super();
        this.aadClientId = aadClientId;
        this.aadClientSecret = aadClientSecret;
        this.aadEndpointInst = aadEndpointInst;
        this.aadTenant = aadTenant;
        this.aadGraphApi = aadGraphApi;
    }

    public void setAadClientId(String aadClientId) {
        this.aadClientId = aadClientId;
    }

    public String getAadClientId() {
        return aadClientId;
    }

    public void setAadClientSecret(String aadClientSecret) {
        this.aadClientSecret = aadClientSecret;
    }

    public String getAadClientSecret() {
        return aadClientSecret;
    }

    public void setAadEndpointInst(String aadEndpointInst) {
        this.aadEndpointInst = aadEndpointInst;
    }

    public String getAadEndpointInst() {
        return aadEndpointInst;
    }

    public void setAadTenant(String aadTenant) {
        this.aadTenant = aadTenant;
    }

    public String getAadTenant() {
        return aadTenant;
    }

    public void setAadGraphApi(String aadGraphApi) {
        this.aadGraphApi = aadGraphApi;
    }

    public String getAadGraphApi() {
        return aadGraphApi;
    }
}