package com.sample.dev;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;

import com.microsoft.graph.authentication.IAuthenticationProvider;
import com.microsoft.graph.http.IHttpRequest;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.requests.extensions.GraphServiceClient;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;

/**
 * @author mgmartinez
 * @version 1.0
 */
public class AADManager {
    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_TYPE = "application/json";

    public static final String HEADER_AUTHORIZARION = "Authorization";

    private static AADManager INSTANCE;
    private AADConnParams aadConnParams;

    public AADManager(AADConnParams aadConnParams) {
        this.aadConnParams = aadConnParams;
    }

    /**
     * Retorna la instancia de la clase
     * @param aadConnParams
     * @return
     */
    public static synchronized AADManager getInstance(AADConnParams aadConnParams) {
        if (INSTANCE == null) {
            INSTANCE = new AADManager(aadConnParams);
        }
        return INSTANCE;
    }

    public void setAadConnParams(AADConnParams aadConnParams) {
        this.aadConnParams = aadConnParams;
    }

    public AADConnParams getAadConnParams() {
        return aadConnParams;
    }

    /**
     * Se obtiene el servicio de autenticacion de ScribeJava oauth 2.0
     * @return
     */
    public OAuth20Service getService() {
        AADLogger.getInstance().writeLog(Level.INFO, "Inicializacion servicio oauth 2.0");
        return new ServiceBuilder(aadConnParams.getAadClientId()).callback(aadConnParams.getAadEndpointInst()).defaultScope(aadConnParams.getAadGraphApi()).apiSecret(aadConnParams.getAadClientSecret()).build(AzureAD20Api.instance(aadConnParams));
    }

    /**
     * Metodo genera token y construye el codigo de autenticacion.
     * @return
     */
    public String getAuthorizationCode() {
        AADLogger.getInstance().writeLog(Level.INFO, "Generando token y codigo de autenticacion para la solicitud HTTP");
        
        final OAuth20Service service = getService();
        final Future<OAuth2AccessToken> authUrl =
            service.getAccessTokenClientCredentialsGrantAsync(aadConnParams.getAadGraphApi());

        StringBuilder authCode = new StringBuilder();

        try {
            authCode.append("Bearer ").append(authUrl.get().getAccessToken());
        } catch (InterruptedException | ExecutionException ex) {
            try {
                throw ex;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return authCode.toString();
    }

    /**
     * Metodo establece el encabezado de autorizacion de solicitud HTTP.
     * @param authCode
     * @return
     */
    public IAuthenticationProvider getAuthProvider(final String authCode) {
        AADLogger.getInstance().writeLog(Level.INFO, "Agregando encabezado de autenticacion para la solicitud HTTP");
        
        IAuthenticationProvider authProvider = new IAuthenticationProvider() {
            @Override
            public void authenticateRequest(IHttpRequest request) {
                try {
                    request.addHeader(HEADER_CONTENT_TYPE, CONTENT_TYPE);
                    request.addHeader(HEADER_AUTHORIZARION, authCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        return authProvider;
    }

    /**
     * Cliente de API Microsoft Graph
     * @return
     */
    public IGraphServiceClient graphClient() {
        String code = getAuthorizationCode();
        IAuthenticationProvider authProvider = getAuthProvider(code);
        System.out.println("Code: " + code + " | AuthProvider: " + authProvider);
        
        return GraphServiceClient.builder().authenticationProvider(authProvider).buildClient();
    }
}

