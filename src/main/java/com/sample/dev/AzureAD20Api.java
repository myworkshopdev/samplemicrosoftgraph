package com.sample.dev;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.oauth.OAuthService;

public class AzureAD20Api extends DefaultApi20 {
    private static AzureAD20Api INSTANCE;
    private AADConnParams aadConnParams;

    public AzureAD20Api(AADConnParams aadConnParams) {
        this.aadConnParams = aadConnParams;
    }

    public static DefaultApi20 instance(AADConnParams aadConnParams) {
        if (INSTANCE == null) {
            INSTANCE = new AzureAD20Api(aadConnParams);
        }
        return INSTANCE;
    }

    @Override
    public String getAccessTokenEndpoint() {
        return aadConnParams.getAadEndpointInst();
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        return null;
    }
}
