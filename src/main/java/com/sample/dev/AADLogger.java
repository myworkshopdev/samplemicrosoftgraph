package com.sample.dev;

import com.microsoft.graph.logger.LoggerLevel;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AADLogger {
    public static final LoggerLevel LOG_LEVEL = LoggerLevel.ERROR;

    public static Logger log;
    public static AADLogger INSTANCE;

    /**
     * Bloque de inicializaci�n antes del constructor predeterminado
     * Configuraciones
     */
    {
        String loggerName = "com.microsoft.graphsample.connect";
        log = Logger.getLogger(loggerName);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.ALL);
        log.addHandler(handler);
        log.setLevel(Level.ALL);
    }

    public AADLogger() {
        super();
    }

    /**
     * M�todo devuelve instancia de la clase
     * @return
     */
    public static synchronized AADLogger getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AADLogger();
        }

        return INSTANCE;
    }

    /**
     * Escribe mensaje de informacion en el servidor
     * @param logLevel
     * @param message
     */
    public void writeLog(Level logLevel, String message) {
        if (LOG_LEVEL == LoggerLevel.DEBUG) {
            log.log(logLevel, message);
        }
    }

    /**
     * Escribe mensaje con excepcion en el servidor
     * @param logLevel
     * @param message
     * @param exception
     */
    public void writeLog(Level logLevel, String message, Exception exception) {
        if (LOG_LEVEL == LoggerLevel.DEBUG) {
            log.log(logLevel, message, exception);
        }
    }
}
