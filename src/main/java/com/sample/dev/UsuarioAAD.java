package com.sample.dev;

import com.google.gson.JsonPrimitive;
import com.microsoft.graph.models.extensions.DirectoryObject;
import com.microsoft.graph.models.extensions.PasswordProfile;
import com.microsoft.graph.models.extensions.User;

import java.sql.CallableStatement;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import java.util.logging.Level;

import javax.naming.Context;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

/**
 * Clase para el acceso a los usuarios del Oracle Internet Directory (AAD)
 * Contiene todos los atributos y metodos asociados a un usuario de AAD.
 * @author mgmartinez
 * @date 19-sept-2014
 * */
public class UsuarioAAD {
    private static final boolean USER_ACCOUNT_ENABLED = true;
    private static final boolean FORCE_CHANGE_PASSWORD_NEXT_SIGN_IN = false;
    
    private AADManager aadManager = null;
    private AADConnParams adConnParams;

    // Codigos de Error manejados por el aplicativos.
    private String SEG_USUARIO_SOLICITUD_INCORRECTA = "SEG_USUARIO_SOLICITUD_INCORRECTA";
    private String SEG_USUARIO_EXISTE = "SEG_USUARIO_EXISTE"; // Posible Mensaje EL usuario ya existe en el directorio LDAP
    private String SEG_USUARIO_EXISTE_EN_GRUPO = "SEG_USUARIO_EXISTE_EN_GRUPO"; // El usuario ya existe en el grupo LDAP
    private String SEG_GRUPO_NO_EXISTE = "SEG_GRUPO_NO_EXISTE"; //  Grupo no existe en el directorio LDAP
    private String SEG_USUARIO_NO_EXISTE = "SEG_USUARIO_NO_EXISTE"; // El usuario no existe en el grupo LDAP
    private String SEG_NO_PERMISO_AAD = "SEG_NO_PERMISO_AAD"; // No hay permiso en el LDAP
    private String SEG_ESTADO_USUARIO_NO_VALIDO = "SEG_ESTADO_USUARIO_NO_VALIDO";

    // Codigos de Error manejados por el AAD.
    private String GSL_PWDMINLENGTH_EXCP =
        "GSL_PWDMINLENGTH_EXCP"; // No cumple con la politica de tamaño minimo en el directorio LDAP
    private String GSL_PWDNUMERIC_EXCP = "GSL_PWDNUMERIC_EXCP"; // Password no cumple con la politica numerica

    public UsuarioAAD(AADConnParams adConnParams) {
        super();
        this.aadManager = new AADManager(adConnParams);
        this.adConnParams = adConnParams;
    }

    public AADConnParams getAADConnParam() {
        return this.adConnParams;
    }

    public void setAADCOnnParam(AADConnParams valor) {
        this.adConnParams = valor;
    }

    public String obtieneAADErrorCode(String errorCode) {
        if (errorCode != null) {
            if (errorCode.toUpperCase().contains("LDAP: ERROR CODE 19")) {
                if (errorCode.toUpperCase().contains(this.GSL_PWDMINLENGTH_EXCP))
                    return this.GSL_PWDMINLENGTH_EXCP;
                if (errorCode.toUpperCase().contains(this.GSL_PWDNUMERIC_EXCP))
                    return this.GSL_PWDNUMERIC_EXCP;
            }
        }
        return errorCode;
    }

    public String crearUsuarioAAD(String mailNickName/*userName*/, String password/*clave*/, String companyName/*compannia*/, String employeeId/*numeroEmpleado*/,
                                  String mail/*correoElectronico*/, String givenName/*nombre*/, String displayName/*nombreCompleto*/, String surname/*apellido*/,
                                  String hireDate/*fechaInicio*/, String fechaVencimiento, String departament/*dependencia*/, String jobTitle/*puesto*/, String userPrincipalName,
                                  String manager) {
        User user = new User();
        user.accountEnabled = USER_ACCOUNT_ENABLED;
        user.companyName = companyName;
        //user.employeeId = employeeId;
        user.givenName = givenName;
        user.surname = surname;
        user.displayName = displayName;
        //user.mail = mail;
        user.mailNickname = mailNickName;
        user.userPrincipalName = userPrincipalName;
        user.passwordPolicies = "DisablePasswordExpiration";
        
        PasswordProfile passwordProfile = new PasswordProfile();
        passwordProfile.password = password;
        passwordProfile.forceChangePasswordNextSignIn = FORCE_CHANGE_PASSWORD_NEXT_SIGN_IN;
        user.passwordProfile = passwordProfile;

        //user.hireDate = stringToCalendar(hireDate);
        user.department = departament;
        user.jobTitle = jobTitle;
        //user.manager = getManager(manager);
        
        try {
            aadManager.graphClient().users().buildRequest().post(user);
        } catch (Exception e) {
            AADLogger.getInstance().writeLog(Level.SEVERE, SEG_USUARIO_SOLICITUD_INCORRECTA);
            return SEG_USUARIO_SOLICITUD_INCORRECTA;
        }
        
        return null;
    }

    public String existeUsuarioEnAAD(String userName, String compannia) throws Exception {
        String rootContext = "";
        return null;
    }

    public String agregarUsuarioAGrupAAD() {
        DirectoryObject directoryObject = new DirectoryObject();
        directoryObject.additionalDataManager().put("@odata.id", new JsonPrimitive("https://graph.microsoft.com/v1.0/directoryObjects/9503be3b-794c-47cb-a21c-25495152dec2"));
        aadManager.graphClient().groups("c0221cff-7d81-4356-ac9b-f5763c75e35b").members().references().buildRequest().post(directoryObject);
        return null;
    }

    public String removerUsuarioDeGrupAAD(String userName, String compannia, String nombreGrupo) {
        return null;
    }

    public String cambiarEstadoUsuarioAAD(String userName, String compannia, String estado, String fechaVencimiento) {
        return null;
    }

    public String cambiarEstadoUsuarioAAD(String userName, String compannia, String estado) {
        return null;
    }

    public String actualizarUsuarioAAD(String userName, String clave, String compannia, String numeroEmpleado,
                                       String correoElectronico, String nombre, String nombreCompleto, String apellido,
                                       String fechaInicio, String fechaVencimiento, String dependencia, String puesto,
                                       String manager) {
        return null;
    }

    public boolean esUsuarioHabilitadoAAD(String userName, String compannia) throws Exception {
        return false;
    }

    public String getParametrosPassword(String atributo) {
        return null;
    }

    public String setParametrosPassword(String atributo, String valor) {
        return null;
    }

    public String obtenerParametrosPassword(String atributo) {
        return null;
    }

    public String desbloquearUsuario(String usuario, String compannia) {
        return null;
    }

    public String esUsuarioBloqueadoAAD(String userName, String compannia) throws Exception {
        return null;
    }

    /**
     * Mrtodo convierte una fecha en String a tipo de dato Calendar.
     * @param dateString
     * @return
     */
    public Calendar stringToCalendar(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        try {
            Date date = sdf.parse(dateString);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            
            return cal;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Metodo obtiene el DirectoryObject del manager para el nuevo usuario, los parametros requeridos pueden ser
     * el id del usuario o el userPrincipalName
     * @param managerId
     * @return
     */
    public DirectoryObject getManager(String managerId) {
        return aadManager.graphClient().users(managerId).buildRequest().get();
    }
}
