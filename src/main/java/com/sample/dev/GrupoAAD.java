package com.sample.dev;

import java.sql.CallableStatement;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;

public class GrupoAAD {
    private AADManager AADM = null;
    private AADConnParams adConnParams;
    private String SEG_GRUPO_EXISTE = "SEG_GRUPO_EXISTE"; // Posible Mensaje EL grupo ya existe en el directorio LDAP
    private String SEG_NOMBRE_GRUPO_REQUERIDO = "SEG_NOMBRE_GRUPO_REQUERIDO"; // Nombre de grupo es requerido.
    private String SEG_DESCRIP_REQUERIDO = "SEG_DESCRIP_REQUERIDO"; // Descripci�n es requerido
    private String SEG_COMPANNIA_REQUERIDO = "SEG_COMPANNIA_REQUERIDO"; // Compa�ia es requerido      // Compa�ia es requerido
    private String SEG_NO_PERMISO_AAD = "SEG_NO_PERMISO_AAD"; // No hay permiso en el LDAP
    public GrupoAAD(AADConnParams adConnParams) {
        super();
        this.AADM = new AADManager(adConnParams);
        this.adConnParams = adConnParams;
    }
    private String obtieneAADErrorCode(String errorCode) {
        return errorCode;
    }
    public String existeGrupoEnAAD(String groupName, String compannia) throws Exception {
        return null;
    }
    public boolean existeUsuarioenGrupoAAD(String userName, String groupName, String compannia) throws Exception {
        return false;
    }
    public String crearGrupoAAD(String nombreGrupo, String descripcion, String compannia, String owner) {
        return null;
    }
    public String removerGrupoAAD(String nombreGrupo, String compannia) {
        return null;
    }
    public String actualizarGrupoAAD(String nombreGrupo, String descripcion, String compannia, String owner) {
        return null;
    }
}